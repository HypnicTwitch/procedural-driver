# Procedural Driver
A project in teaching myself procedural generation and wheel colliders in Unity.

This project started out building procedural terrain based on the Brackeys tutorial
* Perlin Noise: https://www.youtube.com/watch?v=bG0uEXV6aHQ
* Terrain generation based on Perlin Noise https://www.youtube.com/watch?v=vFvwyu_ZKfU

Then moved on to trying to build a car with Unity's Wheel Collider
* Wheel Collider: https://docs.unity3d.com/Manual/class-WheelCollider.html
* Wheel Collider Tutorial: https://docs.unity3d.com/Manual/WheelColliderTutorial.html

I've got the basics structure of a crude driving simulation in place. Now I want to add some tests
* Infallible Code: [Unity Test Player Input](https://www.youtube.com/watch?v=MGx5mb5b3sY)
