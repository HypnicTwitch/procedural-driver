﻿using System;
using UnityEngine;
namespace SimpleCar
{
    public class WheelObject
    {
        private String wheelName;
        private WheelCollider wheelCldr;
        private GameObject wheelMesh;

        //Encapsulation accessor methods of private String, WheelName
        public string WheelName
        {
            get
            {
                return wheelName;
            }

            set
            {
                wheelName = value;
            }
        }

        public WheelObject(WheelCollider wCol, GameObject wMesh)
        {
            wheelCldr = wCol;
            this.wheelMesh = wMesh;
        }

        public WheelObject(string wheelName, WheelCollider wCol, GameObject wMesh)
        {
            this.wheelName = wheelName;
            this.wheelCldr = wCol;
            this.wheelMesh = wMesh;
        }

        /***
         * Apply the wheel collider's data to the visual representation of the wheel
         * This will make a wheel object actually look like it's rotating and turning (if steerable)
         */
        public void updateWheelVisually() {
            Vector3 simplePos;
            Quaternion simpleRot;
            wheelCldr.GetWorldPose(out simplePos, out simpleRot);

            wheelMesh.transform.position = simplePos;
            wheelMesh.transform.rotation = simpleRot;
        }

        /*
         * starting to rethink this, not sure if writing these wrapper functions is the best way to 
         * handle applying input to the wheels
         */

        /***
         * return the steerAngle of this wheelObject's Wheel Collider
         */
        public float getSteerAngle() {
            return wheelCldr.steerAngle;
        }
        /***
         * set the steerAngle of the Wheel Collider
         */
        public void setSteerAngle(float newAngle) {
            wheelCldr.steerAngle = newAngle;
        }

        public void setMotorTorque(float newTorque) {
            wheelCldr.motorTorque = newTorque;
        }

        public void setBrakeTorque(float newBrakeTorque) {
            wheelCldr.brakeTorque = newBrakeTorque;
        }

        /***
         * return the rotation value of the wheel collider
         * FIXME: Really inefficient way too, discards a lot of information
         */
        public Quaternion getWheelRotationAngle() {
            Vector3 wheelPos;
            Quaternion wheelRot;
            wheelCldr.GetWorldPose(out wheelPos, out wheelRot);
            return wheelRot;
        }
        /***
         * same as above, but for the wheel's position
         */
        public Vector3 getWheelPositionValue() {
            Vector3 wheelPos;
            Quaternion wheelRot;
            wheelCldr.GetWorldPose(out wheelPos, out wheelRot);
            return wheelPos;
        }
    }
}
