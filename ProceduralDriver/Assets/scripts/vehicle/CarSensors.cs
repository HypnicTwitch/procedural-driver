﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSensors {

    public float speed;
    public Rigidbody carBody;
	// Use this for initialization
	public void Start () {
        carBody = GameObject.Find("car").GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	public void Update () {
        speed = getCarSpeed();
	}

    public CarSensors()
    {
        speed = 0f;
    }

    public float getCarSpeed()
    {
        if (carBody != null)
        {
            Debug.Log("velocity: " + carBody.velocity.magnitude);
            return carBody.velocity.magnitude;
        } else {
            Debug.Log("Couldn't find the car.");
            return 0;
        }
    }
}
