﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using SimpleCar;

public class SimpleCarController : MonoBehaviour
{
    private const int wheelsPerAxle = 2;
    private List<AxleInfo> axleInfos; // the information about each individual axle
    private Text throttleVal;
    private Text speedVal;
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    //commented out because I am not using the brakes yet
    // public float maxBrakeForce;
    // private Text brakeVal;

    public Steering mySteering;
    public CarSensors sensors;

    public void Start() {

        //playing around with the data and finding objects
        GameObject vehicleWheelColliders = GameObject.Find("wheel_colliders");
        int wheelCount = vehicleWheelColliders.transform.childCount;
        Debug.Log("There are " + wheelCount + " wheel colliders.");
        int axlesPerVehicle = wheelCount / wheelsPerAxle;
        Debug.Log("With " + wheelCount + " wheels, and each axle allowed " + wheelsPerAxle + " wheels, there are "+axlesPerVehicle+" axles.");


        //For now, I'm manually inserting each wheel onto each axle
        axleInfos = new List<AxleInfo>(2);
        AxleInfo frontAxle = new AxleInfo();
        AxleInfo rearAxle = new AxleInfo();
        axleInfos.Add(frontAxle);
        axleInfos.Add(rearAxle);

        //FRONT LEFT wheel
        GameObject frontLeftMesh = GameObject.Find("wheel_mesh_FL");
        frontLeftMesh.transform.Rotate(0, -60, 60, Space.Self);
        WheelObject frontLeftWheel =
          new WheelObject("FL",
                    GameObject.Find("wheel_cldr_FL").GetComponent<WheelCollider>(),
                    frontLeftMesh);
        frontAxle.leftWheel = frontLeftWheel;

        //FRONT RIGHT wheel
        WheelObject frontRightWheel =
          new WheelObject("FR",
                    GameObject.Find("wheel_cldr_FR").GetComponent<WheelCollider>(),
                    GameObject.Find("wheel_mesh_FR"));
        frontAxle.rightWheel = frontRightWheel;

        frontAxle.motor = true;
        frontAxle.brake = true;
        frontAxle.steering = true;

        //REAR LEFT wheel
        WheelObject rearLeftWheel =
          new WheelObject("RL",
                    GameObject.Find("wheel_cldr_RL").GetComponent<WheelCollider>(),
                    GameObject.Find("wheel_mesh_RL"));
        rearAxle.leftWheel = rearLeftWheel;
        //REAR RIGHT wheel
        WheelObject rearRightWheel =
          new WheelObject("RR",
                    GameObject.Find("wheel_cldr_RR").GetComponent<WheelCollider>(),
                    GameObject.Find("wheel_mesh_RR"));
        rearAxle.rightWheel = rearRightWheel;

        rearAxle.motor = false;
        rearAxle.brake = true;
        rearAxle.steering = false;



        throttleVal = GameObject.Find("throttle_value").GetComponent<Text>();
        speedVal = GameObject.Find("speed_value").GetComponent<Text>();
        /* commenting out braking code until I figure out what it's use is
        maxBrakeForce = 600;
        brakeVal = GameObject.Find("brake_value").GetComponent<Text>();
        */

        /*TODO: dynamically add all available wheels to axles and attach to vehicle
        //iterate by the number of wheels per axle, inner loop handles adding individual wheels
        for (int currCollIdx = 0; currCollIdx < wheelCount; )
        {
            AxleInfo currAxle = new AxleInfo();
            //this add all wheels to a vehicle's axle
            for (int currAxlewheelIdx = 0; currAxlewheelIdx < axlesPerVehicle; currAxlewheelIdx++)
            {
                //increment the wheel index here, rather than at the end of the outer loop
                Transform currCldr = vehicleWheelColliders.transform.GetChild(currCollIdx++);
                //my naming convention is "wheel_cldr_FL" for Front Left, "...FR" for Front Right, etc
                //take the last two chars of the whole name to create a quick-and-dirty name for the wheel
                string wheelName = currCldr.name.Substring(currCldr.name.Length - 2);
                WheelObject newWheel =
                    new WheelObject(wheelName,
                                    GameObject.Find("wheel_cldr_" + wheelName).GetComponent<WheelCollider>(),
                                    GameObject.Find("wheel_mesh_" + wheelName));
                Debug.Log("Wheel Collider " + currCollIdx + ": [" + currCldr.name + "]");
                currAxle.addWheelToAxle();//TODO create this method, once more than 'left' and 'right' is supported on an axle
            }
        }
        */
        mySteering = new Steering();
        mySteering.MaxSteeringAngle = 20;

        sensors = new CarSensors();
        sensors.Start();
    }

    public void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = mySteering.MaxSteeringAngle * Input.GetAxis("Horizontal");
        //float brake = maxBrakeForce * Input.GetAxis("Vertical");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            //apply steering
            if (axleInfo.steering)
            {
                mySteering.SetSteerAngle(axleInfo, steering);
            }
            //apply acceleration
            if (axleInfo.motor /*&& motor > 0*/)
            {
                throttleVal.text = motor.ToString();
                axleInfo.leftWheel.setMotorTorque(motor);
                axleInfo.rightWheel.setMotorTorque(motor);
            }
            //apply braking
            //FIXME: doing this is going stop me from going backwards
            // if I can find out my current speed, I can check if it's 0 or less, then go backwards
            /*
            if (axleInfo.brake && motor < 0) {
                brakeVal.text = "Brake Value: " + motor.ToString();
                axleInfo.leftWheel.setBrakeTorque(Math.Abs(motor));
                axleInfo.rightWheel.setBrakeTorque(Math.Abs(motor));
            }
            */

            axleInfo.leftWheel.updateWheelVisually();
            axleInfo.rightWheel.updateWheelVisually();
        }

        //speedVal.text = "Speed Value: " + sensors.getCarSpeed().ToString();
        sensors.Update();

    }

    public void Update()
    {
        speedVal.text = sensors.speed.ToString();
    }

}

