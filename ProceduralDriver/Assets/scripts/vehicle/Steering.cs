﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCar
{
    public class Steering
    {
        private float maxSteeringAngle; // maximum steer angle the wheel can have

        public float MaxSteeringAngle
        {
            get
            {
                return maxSteeringAngle;
            }

            set
            {
                maxSteeringAngle = value;
            }
        }

        public void SetSteerAngle(AxleInfo axleInfo, float steerAngle)
        {
            axleInfo.leftWheel.setSteerAngle(steerAngle);
            axleInfo.rightWheel.setSteerAngle(steerAngle);
        }

    }
}