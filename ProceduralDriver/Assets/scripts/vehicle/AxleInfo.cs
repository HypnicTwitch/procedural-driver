﻿using SimpleCar;
using UnityEngine;

[System.Serializable]
public class AxleInfo
{
    /*
    public WheelCollider leftWheelCollider;
    public GameObject leftWheelMesh;
    public WheelCollider rightWheelCollider;
    public GameObject rightWheelMesh;
    */
    public WheelObject leftWheel;
    public WheelObject rightWheel;
    /* 
     * TODO: at some point I should make this a collection of WheelObjects, this would allow for more
     * unconventional vehicle configurations
     */
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
    public bool brake; // does this axle have brakes?
}