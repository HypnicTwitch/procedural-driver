﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjRotation : MonoBehaviour {

    public float moveSpeed = 3.0f;
    public float jumpForce = 3.0f;
    public Terrain ground;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        translateObjectPos();
        rotateObjectOnZ(moveSpeed);
        HandleMouseXAxis();
        pointToLocationOnTerrain();
        HandleSimpleMouseInput();
	}

    void rotateObjectOnZ(float speed) {
        transform.Rotate(0, 0, speed);
    }

    void translateObjectPos() {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += new Vector3(-moveSpeed, 0f, 0f) * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(moveSpeed, 0f, 0f) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(0f, 0f, moveSpeed) * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.position += new Vector3(0f, 0f, -moveSpeed) * Time.deltaTime;
        }

        if (Input.GetButtonDown("Jump"))
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
        }

    }

    void HandleMouseXAxis()
    {
        if (Input.GetAxis("Mouse X") < 0)
        {
            //Code for action on mouse moving left
            print("Mouse moved left");
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            //Code for action on mouse moving right
            print("Mouse moved right");
        }
    }

    void pointToLocationOnTerrain() {
        if (Input.GetMouseButtonUp(0))
        {
            Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (ground.GetComponent<Collider>().Raycast(clickRay, out hit, Mathf.Infinity))
            {
                transform.LookAt(hit.point);
            }
        }
    }

    private void HandleSimpleMouseInput()
    {
        //0 is left-click, 1 is right click, 2 is middle click
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Left mouse button pressed.");
            Vector3 mousePos = Input.mousePosition;
            Debug.Log(mousePos.x);
            Debug.Log(mousePos.y);
        }
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Right mouse button pressed.");
        }
        if (Input.GetMouseButtonDown(2))
        {
            Debug.Log("Middle mouse button pressed.");
        }
    }
}
